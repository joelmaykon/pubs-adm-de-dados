﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppPubs
{
	public partial class WebFormConsultaEditora : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void Button1_Click(object sender, EventArgs e)
		{
			ServiceReferencePubs.ServicePubsSoapClient aPubs = new ServiceReferencePubs.ServicePubsSoapClient();
			DataSet ds = aPubs.PublisherSelect(TextBox1.Text);
			GridView1.DataSource = ds;
			GridView1.DataBind();
		}
	}
}