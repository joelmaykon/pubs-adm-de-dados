﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;

namespace WebAppPubs.DAL
{
	public class DALPublishers
	{
		private ServiceReferencePubs.ServicePubsSoapClient aPubs;
		public DALPublishers()
		{
			aPubs = new ServiceReferencePubs.ServicePubsSoapClient();
		}
		[DataObjectMethod(DataObjectMethodType.Insert)]
		public void Insert(Modelo.Publishers obj)
		{
			// Chama WebService
			string s = aPubs.PublisherInsert(
			obj.pub_id, obj.pub_name, obj.city, obj.state, obj.country);
		}
		[DataObjectMethod(DataObjectMethodType.Update)]
		public void Update(Modelo.Publishers obj)
		{
			// Chama WebService
			string s = aPubs.PublisherUpdate(
			obj.pub_id, obj.pub_name, obj.city, obj.state, obj.country);
		}
		[DataObjectMethod(DataObjectMethodType.Delete)]
		public void Delete(Modelo.Publishers obj)
		{
			// Chama WebService
			string s = aPubs.PublisherDelete(obj.pub_id);
		}
		[DataObjectMethod(DataObjectMethodType.Select)]
		public List<Modelo.Publishers> SelectAll()
		{
			List<Modelo.Publishers> aListPublishers = this.Select("");
			return aListPublishers;
		}
		[DataObjectMethod(DataObjectMethodType.Select)]
		public List<Modelo.Publishers> Select(string pub_name)
		{
			//if (pub_name == null) pub_name = "";
			// Variavel para armazenar um livro
			Modelo.Publishers aPublishers;
			// Cria Lista Vazia
			List<Modelo.Publishers> aListPublishers = new List<Modelo.Publishers>();
			// Consulta WebService
			DataSet ds = aPubs.PublisherSelect(pub_name);
			// Verifica se retounou resultado
			if (ds.Tables.Count > 0)
			{
				// Pega tabela com resultado da consulta
				DataTable dt = ds.Tables[0];
				// Verifica se retornou linhas
				if (dt.Rows.Count > 0)
				{
					DataRow dr;
					// Acessa cada linha
					for (int i = 0; i < dt.Rows.Count; i++)
					{
						dr = dt.Rows[i];
						// Cria objeto com dados lidos do banco de dados
						aPublishers = new Modelo.Publishers(
						 dr["pub_id"].ToString(),
						dr["pub_name"].ToString(),
						dr["city"].ToString(),
						dr["state"].ToString(),
						dr["country"].ToString()
						);
						// Adiciona o livro lido à lista
						aListPublishers.Add(aPublishers);
					}
				}
			}
			return aListPublishers;
		}
	}
}